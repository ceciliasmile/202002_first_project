``` 
#Q1.本模型是否會隨著銷售量越低，item_category_id猜不準的比例越高？

display(df_tmp.sort_values('All',ascending=False)[['bg_ratio']].plot.bar(stacked=True, figsize=(15,5), title='依照銷售量大到小排列，看看bg_ratio(猜不準)的分佈'))

#隨著商品分類銷售量越低，猜不準比例(bg_ratio)有越高的趨勢，顯示隨著商品銷售量越低會影響模型預估準確率。



#Q2.猜不準的item_category_id前三名為？ 這三名是否因為銷售量較差，所以猜不準？

display(df_tmp.sort_values('bg_ratio',ascending=False).head(3))
#就猜不準比例來看
#item_category_id = 42、79、83
#這三類商品銷售量皆低於中位數，銷售量較差可能導致預估成效較差


#Q3.本模型是否會隨著銷售量越低，city_code猜不準的比例越高？
display(df_tmp.sort_values('All',ascending=False)[['bg_ratio']].plot.bar(stacked=True, figsize=(15,5), title='依照銷售量大到小排列，看看bg_ratio(猜不準)的分佈'))

#隨著city銷售量越低，猜不準比例(bg_ratio)沒有明顯升高或降低趨勢，顯示商品銷售量與模型預估準確率沒有太大關聯。
``` 
![](bg_ratio_All_city.png)


![](bg_ratio_All.png)




